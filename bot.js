var TelegramBot = require('node-telegram-bot-api');
var https = require('https');
var url = require('url');
var spawn = require('child_process').spawn;

var token = '';
var bot = new TelegramBot(token, {polling: true});


bot.on('sticker', function(message) { 
    var chatid = message.chat.id;
    console.log(message);
    console.log(message.sticker.file_id);
    bot.getFileLink(message.sticker.file_id).then(function(link) {
        var parsedLink = url.parse(link);
        bot.sendMessage(chatid, link);
        var dwebp = spawn('dwebp', ['-o', '-', '--', '-']);
        var imgurOptions = {
            hostname: 'api.imgur.com',
            port: 443,
            path: '/3/image',
            method: 'POST',
            headers: {
                Authorization: ''
            }
        };
        var imgurRequest = https.request(imgurOptions, function(res) {
            res.on('data', function(data) {
                console.log('imgur returned data: ' + data);
                var imgurLink = JSON.parse('' + data).data.link;
                imgurLink = imgurLink.replace('http://', 'https://');
                bot.sendMessage(chatid, imgurLink, { disable_web_page_preview: true });
                bot.sendMessage(chatid, '![](' + imgurLink + ')', { disable_web_page_preview: true });
                bot.sendMessage(chatid, '<img src="' + imgurLink + '"></img>', { disable_web_page_preview: true });
            });
        });
        dwebp.stdout.pipe(imgurRequest);
        var telegramOptions = {
            hostname: parsedLink.host,
            path: parsedLink.path,
            method: 'GET'
        };
        var telegramRequest = https.request(telegramOptions, function(res) {
            res.pipe(dwebp.stdin);
        });
        telegramRequest.end();
    });
});
